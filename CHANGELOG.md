# Changelog

## Version 2.1.2 (2020-05-28)

* Improved: Code quality
* Updated: Settings library (Select2 improvements)

## Version 2.1.1

* Updated: Settings library (fixes sysinfo security vulnerability)

## Version 2.1.0

* Bring code up to current standards

## Version 2.0.1

* Only load admin scripts on our settings page!

## Version 2.0.0

* Complete rewrite!
* Completely backwards compatible (custom template-based stuff won't break)
* Now works with custom post types!
* Added Font Awesome icon support

## Version 1.1.2

* Update support link

## Version 1.1.1

* Fix bug with WordPress installed in subdirectory not properly saving settings

## Version 1.1.0

* Moved to class-based structure
* Added proper I18N

## Version 1.0.0

* Initial release
