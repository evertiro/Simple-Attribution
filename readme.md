# Simple Attribution

## Welcome to our GitLab Repository

Simple Attribution is just that... simple! It adds a meta box on post pages
which allows bloggers to specify the name and URL of the site a sourced article
originated from. Assuming both these fields are filled out, it appends the
attribution link to the bottom of the post.

Simple Attribution allows links to be generated in both text and image form,
allowing you to customize it to the feel of your website. Natively, it includes
5 icons which can be used to identify the attribution link instead of the
standard caption (which is editable through the options panel), and custom
icons can be used as well.

Don't like where we put the link? You have the option to disable auto-attribution
and put the link wherever you want it to display simply by adding
&lt;?php echo display_attribution(); ?&gt; to your template!

### Installation

1. You can clone the GitLab repository: `https://gitlab.com/widgitlabs/wordpress/simple-attribution.git`
2. Or download it directly as a ZIP file: `https://gitlab.com/widgitlabs/wordpress/simple-attribution/-/archive/master/simple-attribution-master.zip`

This will download the latest developer copy of Simple Attribution.

### Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/simple-attribution/issues?state=open)!

### Contributions

Anyone is welcome to contribute to Simple Attribution. Please read the
[guidelines for contributing](https://gitlab.com/widgitlabs/wordpress/simple-attribution/-/blob/master/CONTRIBUTING.md)
to this repository.
